/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator1;

import Calculadora.Calcular;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cesarflores
 */
public class calcuTest {
    
    public calcuTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of suma method, of class Calcular.
     */
    @Test
    public void testSuma() {
        System.out.println("suma");
        double a = 4.0;
        double b = 4.0;
        Calcular instance = new Calcular();
        double expResult = 8.0;
        double result = instance.suma(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of resta method, of class Calcular.
     */
    @Test
    public void testResta() {
        System.out.println("resta");
        double a = 3.0;
        double b = 1.0;
        Calcular instance = new Calcular();
        double expResult = 2.0;
        double result = instance.resta(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of multiplicacion method, of class Calcular.
     */
    @Test
    public void testMultiplicacion() {
        System.out.println("multiplicacion");
        double a = 4.0;
        double b = 4.0;
        Calcular instance = new Calcular();
        double expResult = 16.0;
        double result = instance.multiplicacion(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of division method, of class Calcular.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        double a = 10.0;
        double b = 2.0;
        Calcular instance = new Calcular();
        double expResult = 5.0;
        double result = instance.division(a, b);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
